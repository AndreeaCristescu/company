package com.sda.company.controler;


import com.sda.company.components.CustomFakerEmployee;
import com.sda.company.model.Company;
import com.sda.company.model.Employee;
import com.sda.company.service.EmployeeService;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/employees")
@ControllerAdvice
public class EmployeeController {

    private final EmployeeService employeeService;
    private final CustomFakerEmployee customFakerEmployee;


    @Autowired
    public EmployeeController(EmployeeService employeeService, CustomFakerEmployee customFakerEmployee) {
        this.employeeService = employeeService;
        this.customFakerEmployee = customFakerEmployee;
    }

    @PostMapping("/createEmployee")
    public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee) {
        return ResponseEntity.ok(employeeService.create(employee));
    }

    @GetMapping("/getAllEmployees")
    public ResponseEntity<List<Employee>> getAllEmployees() {
        return ResponseEntity.ok(employeeService.getAll());
    }

    @GetMapping("/getAllEmployeesPaginated")
    public ResponseEntity<List<Employee>> getAllPaginated(
            @RequestParam(defaultValue = "0") Integer pageNumber,
            @RequestParam(defaultValue = "50") Integer pageSize,
            @RequestParam(defaultValue = "firstName") String sortBy) {
        return ResponseEntity.ok(employeeService.getAllPaginated(pageNumber, pageSize, sortBy));
    }

    @GetMapping("/findByFirstName")
    public ResponseEntity<Employee> findByFirstName(@RequestParam String firstName) {
        return ResponseEntity.ok(employeeService.findByFirstName(firstName));
    }

    @GetMapping("/populateEmployee")
    public ResponseEntity<String> populate() {
        return ResponseEntity.ok(employeeService.populate(customFakerEmployee.createDummyEmployeeList()));
    }

    @PutMapping("/hire")
    public ResponseEntity<Employee> hireEmployee(@RequestParam @NotNull Integer employeeId,
                                                 @RequestParam @NotNull Integer companyId){
        return ResponseEntity.ok(employeeService.hire(employeeId, companyId));
    }
}
