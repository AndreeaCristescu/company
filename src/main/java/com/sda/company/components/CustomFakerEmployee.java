package com.sda.company.components;

import com.github.javafaker.Faker;
import com.sda.company.model.Company;
import com.sda.company.model.Employee;

import java.util.ArrayList;
import java.util.List;

public class CustomFakerEmployee {

    public List<Employee> createDummyEmployeeList(){
        Faker faker = new Faker();
        List<Employee> employeeList = new ArrayList<>();

        for(int i = 0; i < 100; i++){
            Employee employee= new Employee();
            employee.setFirstName(faker.name().firstName());
            employee.setLastName(faker.name().lastName());
            employee.setPhone(faker.phoneNumber().phoneNumber());
            employee.setEmail(faker.bothify("?????##@yahoo.com"));
            employee.setAddress(faker.address().fullAddress());
            employee.setHired(faker.random().nextBoolean());
            employee.setPersonalNumericCode(faker.number().randomNumber(11, true));

            employeeList.add(employee);

        }
        return employeeList;
    }
}



