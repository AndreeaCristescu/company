package com.sda.company.service.impl;

import com.sda.company.exception.CompanyNotFoundException;
import com.sda.company.exception.EmployeeNotFoundException;
import com.sda.company.model.Company;
import com.sda.company.model.Employee;
import com.sda.company.repository.EmployeeRepository;
import com.sda.company.service.CompanyService;
import com.sda.company.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final CompanyService companyService;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository, CompanyService companyService){
        this.employeeRepository= employeeRepository;
        this.companyService = companyService;
    }


    @Override
    public Employee create(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public String populate(List<Employee> employeesList) {
        List<Employee> result =(List<Employee>) employeeRepository.saveAll(employeesList);
         if(result.isEmpty()){
             return "There is a problem";
         }else{
             return "EmployeeList has been created";
         }
    }

    @Override
    public List<Employee> getAll() {
        return (List<Employee>)employeeRepository.findAll();
    }

    @Override
    public List<Employee> getAllPaginated(Integer pageNumber, Integer pageSize, String sortBy) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(sortBy));
        Page<Employee> employeePage = employeeRepository.findAll(pageable);

        return employeePage.getContent();
    }

    @Override
    public Employee findByFirstName(String firstName) {
        return employeeRepository.findByFirstName(firstName)
                .orElseThrow(() -> new EmployeeNotFoundException("Employee with name " + firstName + "does not exist"));
    }

    @Override
    public Employee hire(Integer employeeId, Integer companyId) {
        Company company= companyService.findById(companyId)
                .orElseThrow(() -> new CompanyNotFoundException("Company not found"));

//        Employee employee= employeeRepository
//                .findById(employeeId).orElseThrow(() -> new EmployeeNotFoundException("Employee not found"));
//
//        employee.setCompany(company);
//        employee.setHired(true);
//
//        return employeeRepository.save(employee);

//        sau varianta mai rapida:

        return employeeRepository.findById(employeeId).map(employee -> {
            employee.setCompany(company);
            employee.setHired(true);

            return employeeRepository.save(employee);
        }).orElseThrow(() -> new EmployeeNotFoundException("Employee not found"));
    }


    @Override
    public Optional<Employee> findById(Integer id) {
        return employeeRepository.findById(id);
    }
}
